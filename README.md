# buffer

A stream buffer backed by `tempfile.SpooledTemporaryFile`. 

[Click here to learn more about `buffer`](https://alexdelorenzo.dev/programming/2019/04/14/buffer).

[Click here for the Rust version of this library](https://gitlab.com/thismachinechills/buffers-rs).